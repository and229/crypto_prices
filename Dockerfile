FROM python:3.9

ENV PYTHONPATH "${PYTHONPATH}:/app/app"
ENV PYTHONUNBUFFERED 1

EXPOSE 38034

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

COPY ./app /app/app

WORKDIR /app

CMD uvicorn app.main:app --host 0.0.0.0 --port 8000 --reload
