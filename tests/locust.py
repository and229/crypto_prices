from locust import HttpUser, task, between


BASE = "http://localhost:8000"


class TestUser(HttpUser):
    wait_time = between(0.1, 0.5)

    @task
    def get_prices(self):
        self.client.get(BASE + "/courses")
