import logging

from fastapi import APIRouter, status
from fastapi.exceptions import HTTPException

from app.services.repository import repository
from app.schemas.trading_pair import TradingPairPrice
from app.core.constants import TRADING_PAIR_LIST


router = APIRouter(prefix="", tags=["Prices"])
logger = logging.getLogger("prices")


@router.get("/courses", response_model=list[TradingPairPrice])
async def get_all_prices() -> list[TradingPairPrice]:
    prices = []
    for trading_pair in TRADING_PAIR_LIST:
        price_data = await repository.get(trading_pair)
        if price_data is None:
            logger.warning(f"No data for {trading_pair=}")
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Данных для запрашиваемой валютной пары нет: {trading_pair}",
            )
        prices.append(TradingPairPrice(trading_pair=trading_pair, price=1.1))

    return prices


@router.get("/{trading_pair}")
async def get_price(trading_pair: str) -> TradingPairPrice:
    price_data = await repository.get(trading_pair)
    if price_data is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Данных для запрашиваемой валютной пары нет",
        )

    return TradingPairPrice(trading_pair=trading_pair, price=price_data)
