import asyncio
import logging

from fastapi import FastAPI

from app.crypto_prices.views import router as prices_router
from app.tasks.update_prices import run_update_tasks


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("prices")


def create_app() -> FastAPI:
    app = FastAPI()
    app.include_router(prices_router)

    return app


app = create_app()


@app.on_event("startup")
async def start_event():
    logger.info("Start update price tasks")
    asyncio.create_task(run_update_tasks())
    logger.info("End update price tasks")
