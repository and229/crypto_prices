from pydantic import BaseModel


class TradingPairPrice(BaseModel):
    trading_pair: str
    price: float
