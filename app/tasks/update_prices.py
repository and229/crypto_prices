import asyncio
import json
import logging

from websockets import connect

from app.core.constants import (
    BINANCE_WEBSOCKET_BASE,
    OKX_WEBSOCKET_BASE,
)
from app.services.repository import repository
from app.services.client import ExchangeClient
from app.core.constants import (
    TRADING_PAIR_LIST,
    TRADING_PAIR_FORMAT_MAP,
)

logger = logging.getLogger("prices")


async def recv_message(websocket):
    resp = await websocket.recv()
    return resp


async def binance_update_prices_task(symbols: list[str]):
    url = BINANCE_WEBSOCKET_BASE
    logger.debug(f"Binance: Connecting to {url}")
    async with connect(url) as websocket:
        logger.debug(f"Binance: Connected to {url} as {websocket}")
        subscribe_reqv = {
            "method": "SUBSCRIBE",
            "params": [f"{symbol.lower()}@kline_1s".replace("-", "") for symbol in symbols],
            "id": 1,
        }
        send_data = json.dumps(subscribe_reqv)
        logger.debug(f"Binance: Subscribe request: {subscribe_reqv}")
        await websocket.send(send_data)
        try:
            message = await asyncio.wait_for(recv_message(websocket), timeout=5.0)
            logger.debug(f"Binance: Subscribe response {message}")
            while True:
                message = await asyncio.wait_for(recv_message(websocket), timeout=50.0)
                logger.debug(f"Binance: Got message {message}")
                message = json.loads(message)
                trading_pair = TRADING_PAIR_FORMAT_MAP[message["s"]]
                exists = await repository.is_exists(trading_pair)
                logger.debug(f'Binance: {trading_pair} {exists=}')
                if not exists:
                    try:
                        logger.debug(f'Binance: update {trading_pair}')
                        await repository.update(trading_pair, float(message["k"]["o"]))
                    except KeyError as e:
                        logger.exception(message, exc_info=e)
        except asyncio.TimeoutError:
            logger.warning(f"Binance: Read from {url} timeout")


async def okh_update_price_task(symbol: str):
    url = OKX_WEBSOCKET_BASE
    logger.debug(f"OKX: Connecting to socket {url}")
    async with connect(url) as websocket:
        ticker = f"{symbol}-SWAP"
        subscribe_reqv = {
            "op": "subscribe",
            "args": [
                {
                    "channel": "candle1m",
                    "instId": ticker,
                }
            ],
        }
        send_data = json.dumps(subscribe_reqv)
        logger.debug(f"OKX: Subscribe request: {subscribe_reqv}")
        await websocket.send(send_data)
        try:
            message = await asyncio.wait_for(recv_message(websocket), timeout=5.0)
            logger.debug(f"OKX: Subscribe response {message}")
            data = json.loads(message)
            if data["event"] == "error":
                logger.warning(f"OKX: No such ticker on OKX: {ticker}")
                return
            else:
                while True:
                    message = await asyncio.wait_for(recv_message(websocket), timeout=50.0)
                    message = json.loads(message)
                    logger.debug(f"OKX: Got message {message}")
                    trading_pair = message["arg"]["instId"].replace("-", "").replace("SWAP", "")
                    trading_pair = TRADING_PAIR_FORMAT_MAP[trading_pair]
                    if not await repository.is_exists(symbol):
                        logger.debug(f'{trading_pair=} {message["data"][0][1]=}')
                        await repository.update(trading_pair, float(message["data"][0][1]))
        except asyncio.TimeoutError:
            logger.warning(f"OKX: Read from {url} timeout")


async def okx_update_prices_task(symbols: list[str]):
    tasks = [
        asyncio.create_task(okh_update_price_task(symbol))
        for symbol in symbols
    ]
    await asyncio.gather(*tasks)


async def client_update_price_task(client: ExchangeClient, symbol: str):
    while True:
        try:
            exists = await repository.is_exists(symbol)
            logger.debug(f'Binance: {symbol} {exists=}')
            if not exists:
                price = await client.get(symbol)
                await repository.update(symbol, price)
        except Exception as e:
            logger.warning(f"Read from {client} error: {e}")
        await asyncio.sleep(1.0)


async def client_update_prices_task(symbols: list[str]):
    client = ExchangeClient()
    tasks = [
        asyncio.create_task(client_update_price_task(client, symbol))
        for symbol in symbols
    ]
    await asyncio.gather(*tasks)


async def run_update_tasks():
    tasks = [
        binance_update_prices_task(TRADING_PAIR_LIST),
        okx_update_prices_task(TRADING_PAIR_LIST),
        client_update_prices_task(TRADING_PAIR_LIST),
    ]
    await asyncio.gather(*tasks)
