import asyncio
from typing import Optional

import aioredis

from app.core.constants import PRICE_EXPIRATION_TIME
from app.core.settings import settings


class PriceRepository:
    def __init__(self):
        self._redis = aioredis.from_url(settings.redis_url)

    async def is_exists(self, trading_pair: str) -> bool:
        return bool(await self._redis.ttl(trading_pair) >= 25)

    async def update(self, trading_pair: str, value: float):
        await self._redis.setex(trading_pair, PRICE_EXPIRATION_TIME, value)

    async def get(self, trading_pair: str) -> Optional[float]:
        value = await self._redis.get(trading_pair)
        if value is not None:
            return float(value)

        return None


repository = PriceRepository()


if __name__ == "__main__":
    async def main():
        repo = PriceRepository()
        await repo.update("BTC-KZT", 123.4)
        print(await repo.get("BTC-KZT"))

    asyncio.run(main())
