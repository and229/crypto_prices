import asyncio
from typing import Optional
import logging

import httpx

from app.core.constants import COINBASE_API_BASE

logger = logging.getLogger("prices")


class ExchangeClient:
    def __init__(self):
        self._base_url = COINBASE_API_BASE
        self._client = httpx.AsyncClient()

    async def get(self, symbol: str) -> Optional[float]:
        endpoint = f"/prices/{symbol}/buy"
        try:
            logger.debug(f"{(self._base_url + endpoint)=}")
            resp = await self._client.get(self._base_url + endpoint, timeout=5)
            return resp.json()["data"]["amount"]
        except Exception as e:
            print("Connection error while making request to %s: %s", endpoint, e)
            return None


if __name__ == "__main__":
    async def main():
        client = ExchangeClient()
        print(await client.get("BTC-KZT"))
        print(await client.get("BTC-GEL"))
        print(await client.get("ETH-RUB"))
        print(await client.get("ETH-USD"))
        print(await client.get("USDT-EUR"))
        print(await client.get("USDT-KZT"))
        print(await client.get("USDT-GEL"))
        print(await client.get("USDT-RUB"))
        print(await client.get("USDT-USD"))

    asyncio.run(main())
